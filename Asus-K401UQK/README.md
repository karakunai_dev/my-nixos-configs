# ReadMe
This folder contains `hardware-configuration.nix` for Asus K401UQK laptop that I've been using since October 11th, 2020 on NixOS 20.03. For more information about the specs of this laptop, please refer to [this page](https://www.asus.com/Notebooks/K401UQ/specification/) for more information.

For the `configuration.nix` file, it contains a pretty standard KDE setup along with some packages listed below.

---
## Listed Pakcages

    wget vim firefox vivaldi chromium mpv gparted encryptr vivaldi-widevine vivaldi-ffmpeg-codecs steam steam-run docker-compose discord qview gitFull spectacle youtube-dl nodejs-12_x spotify ark htop minecraft
